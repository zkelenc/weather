<?php

return array(

	'copyright' => 'Copyright &copy; Weatherbound.',
	'tasks_recommendation' => 'Activities you should consider today:',
	'fact_title' => 'Did you know?',
    'city_pick_title' => 'Click on the map to display weather for your desired location',
    'city_pick_accept' => 'Confirm marked location',
    'city_pick_success' => 'Location successfully selected.',

    //Days
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',
    'Sunday' => 'Sunday'
);
