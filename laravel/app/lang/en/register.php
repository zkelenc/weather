<?php

return array(

    'title' => 'Register',
    'username' => 'Email',
    'password' => 'Password',
    'retype' => 'Re-type',
    'button' => 'Register',
    'language' => 'Language',

    'languages' => array(
        'en' => 'English',
        'si' => 'Slovenian'
    ),

    'confirm' => 'An confirmation email has been sent to your email address'

);