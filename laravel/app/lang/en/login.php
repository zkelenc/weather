<?php

return array(

    'title' => 'Log in',
    'username' => 'Email',
    'password' => 'Password',
    'incorrect_credentials' => 'Username or password does not match.',
    'button' => 'Log in'

);