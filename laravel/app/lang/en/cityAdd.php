<?php

return array(

    'title' => "Add city",
    'city' => "City: ",
    'search' => "Find city",
    'warning' => "Please write the input in format City, Country",
    'city_found' => "Found a city named",
    'add_button' => "Add now",
    'save' => 'Save',
    'javascript_code' => "searching_text = \"Searching\"; no_find_text = \"Can't find city\";
        warning_text = \"Please write the input in format City, Country\"; search_text = \"Find city\";
        add_text = \"Add now\"; adding_text = \"Adding\"; complete_text = \"City added\""
);