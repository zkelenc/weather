<?php

return array(

    'clear_sky' => "Clear sky",
    'few_clouds' => "Partially cloudy",
    'scattered_clouds' => "Scattered clouds",
    'broken_clouds' => "Broken clouds",
    'shower_rain' => "Showers",
    'rain' => "Raining",
    'thunderstorm' => "Thunderstorms",
    'snow' => "Snowing",
    'mist' => "Misty"
);