<?php

return array(

    'login' => 'Log in',
    'profile' => 'Profile',
    'register' => 'Sign up',
    'logout' => 'Log out',
    'options' => 'Options',
    'access' => 'Access',
    'home' => 'Home',
    'forecast' => 'Forecast',
    'language' => 'Language',
    'slovene' => 'Slovenian',
    'english' => 'English',
    'welcome' => 'Welcome back',
    'example_generator' => 'Clothes generator',
    'tasks_generator' => 'Tasks generator',
    'city' => 'Select city',
    'tasks' => 'Plan activities',

    'dont_recommend' => 'Not recommended',
    


    'clothes_error' => 'There was a problem suiting you up. Using fallback clothes.',
    'forecast_error' => 'No city was detected, please try again.',

    'name' => 'Name',
    'surname' => 'Surname',
    'email' => 'Email',
    'avatar' => 'Avatar',
    'selected_city' => 'Selected city',
    'view3D' => '3D View',

    'generic_error' => "Something went wrong, please try again."
);