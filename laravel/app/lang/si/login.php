<?php

return array(

    'title' => 'Prijava',
    'username' => 'Email',
    'password' => 'Geslo',
    'incorrect_credentials' => 'Uporabniško ime ali geslo ni pravilno.',
    'button' => 'Prijava'

);