<?php

return array(

    'login' => 'Prijava',
    'signup' => 'Registracija',
    'profile' => 'Profil',
    'logout' => 'Odjava',
    'options' => 'Nastavitve',
    'access' => 'Dostop',
    'home' => 'Domov',
    'forecast' => 'Napoved',
    'language' => 'Jezik',
    'slovene' => 'Slovenski',
    'english' => 'Angleški',
    'welcome' => 'Dobrodošli nazaj',
    'example_generator' => 'Generator oblačil',
    'tasks_generator' => 'Generator aktivnosti',
    'city' => 'Izberi mesto',
    'tasks' => 'Planer aktivnosti',

    'dont_recommend' => 'Ne priporočamo',

    'clothes_error' => 'Težava pri oblačenju, uporabljam privzeta oblačila.',
    'forecast_error' => 'Zaznali nismo nobenega mesta, prosim poskusite ponovno.',

    'name' => 'Ime',
    'surname' => 'Priimek',
    'email' => 'Email',
    'avatar' => 'Avatar',
    'selected_city' => 'Izbrano mesto',
    'view3D' => '3D pogled', 


    'generic_error' => "Prišlo je do napake, prosim poskusite ponovno."
);