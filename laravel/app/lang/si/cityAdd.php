<?php

return array(

    'title' => "Dodaj mesto",
    'city' => "Mesto: ",
    'search' => "Poišči mesto",
    'warning' => "Prosim vpiši v formatu mesto, država",
    'city_found' => "Našel mesto ",
    'add_button' => "Dodaj zdaj",
    'save' => 'Shrani',
    'javascript_code' => "searching_text = \"Iščem\"; no_find_text = \"Ne najdem mesta\";
        warning_text = \"Prosim vpiši v formatu mesto, država\"; search_text = \"Poišči mesto\";
        add_text = \"Dodaj zdaj\"; adding_text = \"Dodajam\"; complete_text = \"Mesto dodano\""
);