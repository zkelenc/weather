<?php

return array(

    'clear_sky' => "Jasno",
    'few_clouds' => "Delno oblačno",
    'scattered_clouds' => "Razpršena oblačnost",
    'broken_clouds' => "Delno oblačno",
    'shower_rain' => "Plohe",
    'rain' => "Dežuje",
    'thunderstorm' => "Nevihta",
    'snow' => "Sneži",
    'mist' => "Megleno"
);