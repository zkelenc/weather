<?php

return array(

	'copyright' => 'Weatherbound &copy; Vse pravice pridržane.',
	'tasks_recommendation' => 'Danes priporočamo naslednje aktivnosti:',
	'fact_title' => 'Ali ste vedeli?',
    'city_pick_title' => 'Na zemljevidu izberi mesto, za katerega želite podatke o vremenu',
    'city_pick_accept' => 'Potrdi izbrano mesto',
    'city_pick_success' => 'Lokacija uspešno izbrana.',

    //Days
    'Monday' => 'Ponedeljek',
    'Tuesday' => 'Torek',
    'Wednesday' => 'Sreda',
    'Thursday' => 'Četrtek',
    'Friday' => 'Petek',
    'Saturday' => 'Sobota',
    'Sunday' => 'Nedelja'
);
