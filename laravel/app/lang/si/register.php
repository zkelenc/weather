<?php

return array(

    'title' => 'Registracija',
    'username' => 'Email',
    'password' => 'Geslo',
    'retype' => 'Ponovite geslo',
    'button' => 'Registracija',
    'language' => 'Jezik',

    'languages' => array(
        'en' => 'Angleški',
        'si' => 'Slovenski'
    ),

    'confirm' => 'Potrditven email je bil poslan na vaš e-poštni naslov.'
);