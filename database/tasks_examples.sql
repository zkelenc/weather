-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Gostitelj: localhost
-- Čas nastanka: 03. jun 2014 ob 08.51
-- Različica strežnika: 5.6.12-log
-- Različica PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Zbirka podatkov: `development`
--

-- --------------------------------------------------------

--
-- Struktura tabele `tasks_examples`
--

CREATE TABLE IF NOT EXISTS `tasks_examples` (
  `temperature` int(11) NOT NULL,
  `weatherCondition` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `activityType` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1300 ;

--
-- Odloži podatke za tabelo `tasks_examples`
--

INSERT INTO `tasks_examples` (`temperature`, `weatherCondition`, `activityType`, `id`) VALUES
(9, 'clear_sky', 2, 1),
(0, 'clear_sky', 2, 2),
(8, 'clear_sky', 2, 3),
(1, 'clear_sky', 2, 4),
(7, 'clear_sky', 2, 5),
(2, 'clear_sky', 2, 6),
(6, 'clear_sky', 2, 7),
(3, 'clear_sky', 2, 8),
(5, 'clear_sky', 2, 9),
(4, 'clear_sky', 2, 10),
(9, 'clear_sky', 4, 11),
(0, 'clear_sky', 4, 12),
(8, 'clear_sky', 4, 13),
(1, 'clear_sky', 4, 14),
(7, 'clear_sky', 4, 15),
(2, 'clear_sky', 4, 16),
(6, 'clear_sky', 4, 17),
(3, 'clear_sky', 4, 18),
(5, 'clear_sky', 4, 19),
(4, 'clear_sky', 4, 20),
(4, 'clear_sky', 4, 21),
(0, 'clear_sky', 4, 22),
(9, 'clear_sky', 4, 23),
(1, 'clear_sky', 4, 24),
(8, 'clear_sky', 4, 25),
(2, 'clear_sky', 4, 26),
(7, 'clear_sky', 4, 27),
(3, 'clear_sky', 4, 28),
(6, 'clear_sky', 4, 29),
(4, 'clear_sky', 4, 30),
(5, 'clear_sky', 4, 31),
(5, 'clear_sky', 4, 32),
(4, 'clear_sky', 4, 33),
(6, 'clear_sky', 4, 34),
(3, 'clear_sky', 4, 35),
(7, 'clear_sky', 4, 36),
(2, 'clear_sky', 4, 37),
(8, 'clear_sky', 4, 38),
(1, 'clear_sky', 4, 39),
(9, 'clear_sky', 4, 40),
(0, 'clear_sky', 4, 41),
(10, 'clear_sky', 4, 42),
(-1, 'clear_sky', 4, 43),
(0, 'clear_sky', 4, 44),
(9, 'clear_sky', 4, 45),
(1, 'clear_sky', 4, 46),
(8, 'clear_sky', 4, 47),
(2, 'clear_sky', 4, 48),
(7, 'clear_sky', 4, 49),
(3, 'clear_sky', 4, 50),
(6, 'clear_sky', 4, 51),
(4, 'clear_sky', 4, 52),
(5, 'clear_sky', 4, 53),
(5, 'clear_sky', 4, 54),
(4, 'clear_sky', 4, 55),
(9, 'clear_sky', 5, 56),
(0, 'clear_sky', 5, 57),
(8, 'clear_sky', 5, 58),
(1, 'clear_sky', 5, 59),
(7, 'clear_sky', 5, 60),
(2, 'clear_sky', 5, 61),
(6, 'clear_sky', 5, 62),
(3, 'clear_sky', 5, 63),
(5, 'clear_sky', 5, 64),
(4, 'clear_sky', 5, 65),
(4, 'clear_sky', 5, 66),
(0, 'clear_sky', 5, 67),
(9, 'clear_sky', 5, 68),
(1, 'clear_sky', 5, 69),
(8, 'clear_sky', 5, 70),
(2, 'clear_sky', 5, 71),
(7, 'clear_sky', 5, 72),
(3, 'clear_sky', 5, 73),
(6, 'clear_sky', 5, 74),
(4, 'clear_sky', 5, 75),
(5, 'clear_sky', 5, 76),
(5, 'clear_sky', 5, 77),
(4, 'clear_sky', 5, 78),
(6, 'clear_sky', 5, 79),
(3, 'clear_sky', 5, 80),
(7, 'clear_sky', 5, 81),
(2, 'clear_sky', 5, 82),
(8, 'clear_sky', 5, 83),
(1, 'clear_sky', 5, 84),
(9, 'clear_sky', 5, 85),
(0, 'clear_sky', 5, 86),
(10, 'clear_sky', 5, 87),
(-1, 'clear_sky', 5, 88),
(0, 'clear_sky', 5, 89),
(9, 'clear_sky', 5, 90),
(1, 'clear_sky', 5, 91),
(8, 'clear_sky', 5, 92),
(2, 'clear_sky', 5, 93),
(7, 'clear_sky', 5, 94),
(3, 'clear_sky', 5, 95),
(6, 'clear_sky', 5, 96),
(4, 'clear_sky', 5, 97),
(5, 'clear_sky', 5, 98),
(5, 'clear_sky', 5, 99),
(4, 'clear_sky', 5, 100),
(9, 'scattered_clouds', 1, 101),
(0, 'scattered_clouds', 1, 102),
(8, 'scattered_clouds', 1, 103),
(1, 'scattered_clouds', 1, 104),
(7, 'scattered_clouds', 1, 105),
(2, 'scattered_clouds', 1, 106),
(6, 'scattered_clouds', 1, 107),
(3, 'scattered_clouds', 1, 108),
(5, 'scattered_clouds', 1, 109),
(4, 'scattered_clouds', 1, 110),
(4, 'scattered_clouds', 1, 111),
(0, 'scattered_clouds', 1, 112),
(9, 'scattered_clouds', 1, 113),
(1, 'scattered_clouds', 1, 114),
(8, 'scattered_clouds', 1, 115),
(2, 'scattered_clouds', 1, 116),
(7, 'scattered_clouds', 1, 117),
(3, 'scattered_clouds', 1, 118),
(6, 'scattered_clouds', 1, 119),
(4, 'scattered_clouds', 1, 120),
(5, 'scattered_clouds', 1, 121),
(5, 'scattered_clouds', 1, 122),
(4, 'scattered_clouds', 1, 123),
(6, 'scattered_clouds', 1, 124),
(3, 'scattered_clouds', 1, 125),
(7, 'scattered_clouds', 1, 126),
(2, 'scattered_clouds', 1, 127),
(8, 'scattered_clouds', 1, 128),
(1, 'scattered_clouds', 1, 129),
(9, 'scattered_clouds', 1, 130),
(9, 'scattered_clouds', 4, 131),
(0, 'scattered_clouds', 4, 132),
(8, 'scattered_clouds', 4, 133),
(1, 'scattered_clouds', 4, 134),
(7, 'scattered_clouds', 4, 135),
(2, 'scattered_clouds', 4, 136),
(6, 'scattered_clouds', 4, 137),
(3, 'scattered_clouds', 4, 138),
(5, 'scattered_clouds', 4, 139),
(4, 'scattered_clouds', 4, 140),
(4, 'scattered_clouds', 4, 141),
(0, 'scattered_clouds', 4, 142),
(9, 'scattered_clouds', 4, 143),
(1, 'scattered_clouds', 4, 144),
(8, 'scattered_clouds', 4, 145),
(2, 'scattered_clouds', 4, 146),
(7, 'scattered_clouds', 4, 147),
(3, 'scattered_clouds', 4, 148),
(6, 'scattered_clouds', 4, 149),
(4, 'scattered_clouds', 4, 150),
(5, 'scattered_clouds', 4, 151),
(5, 'scattered_clouds', 4, 152),
(4, 'scattered_clouds', 4, 153),
(6, 'scattered_clouds', 4, 154),
(3, 'scattered_clouds', 4, 155),
(7, 'scattered_clouds', 4, 156),
(2, 'scattered_clouds', 4, 157),
(8, 'scattered_clouds', 4, 158),
(1, 'scattered_clouds', 4, 159),
(9, 'scattered_clouds', 4, 160),
(9, 'scattered_clouds', 5, 161),
(0, 'scattered_clouds', 5, 162),
(8, 'scattered_clouds', 5, 163),
(1, 'scattered_clouds', 5, 164),
(7, 'scattered_clouds', 5, 165),
(2, 'scattered_clouds', 5, 166),
(6, 'scattered_clouds', 5, 167),
(3, 'scattered_clouds', 5, 168),
(5, 'scattered_clouds', 5, 169),
(4, 'scattered_clouds', 5, 170),
(4, 'scattered_clouds', 5, 171),
(0, 'scattered_clouds', 5, 172),
(9, 'scattered_clouds', 5, 173),
(1, 'scattered_clouds', 5, 174),
(8, 'scattered_clouds', 5, 175),
(2, 'scattered_clouds', 5, 176),
(7, 'scattered_clouds', 5, 177),
(3, 'scattered_clouds', 5, 178),
(6, 'scattered_clouds', 5, 179),
(4, 'scattered_clouds', 5, 180),
(5, 'scattered_clouds', 5, 181),
(5, 'scattered_clouds', 5, 182),
(4, 'scattered_clouds', 5, 183),
(6, 'scattered_clouds', 5, 184),
(3, 'scattered_clouds', 5, 185),
(7, 'scattered_clouds', 5, 186),
(2, 'scattered_clouds', 5, 187),
(8, 'scattered_clouds', 5, 188),
(1, 'scattered_clouds', 5, 189),
(9, 'scattered_clouds', 5, 190),
(0, 'scattered_clouds', 5, 191),
(10, 'scattered_clouds', 5, 192),
(-1, 'scattered_clouds', 5, 193),
(0, 'scattered_clouds', 5, 194),
(9, 'scattered_clouds', 5, 195),
(1, 'scattered_clouds', 5, 196),
(8, 'scattered_clouds', 5, 197),
(2, 'scattered_clouds', 5, 198),
(7, 'scattered_clouds', 5, 199),
(3, 'scattered_clouds', 5, 200),
(9, 'rain', 1, 201),
(0, 'rain', 1, 202),
(8, 'rain', 1, 203),
(1, 'rain', 1, 204),
(7, 'rain', 1, 205),
(2, 'rain', 1, 206),
(6, 'rain', 1, 207),
(3, 'rain', 1, 208),
(5, 'rain', 1, 209),
(4, 'rain', 1, 210),
(4, 'rain', 1, 211),
(0, 'rain', 1, 212),
(9, 'rain', 1, 213),
(1, 'rain', 1, 214),
(8, 'rain', 1, 215),
(2, 'rain', 1, 216),
(7, 'rain', 1, 217),
(3, 'rain', 1, 218),
(6, 'rain', 1, 219),
(4, 'rain', 1, 220),
(5, 'rain', 1, 221),
(5, 'rain', 1, 222),
(4, 'rain', 1, 223),
(6, 'rain', 1, 224),
(3, 'rain', 1, 225),
(7, 'rain', 1, 226),
(2, 'rain', 1, 227),
(8, 'rain', 1, 228),
(1, 'rain', 1, 229),
(9, 'rain', 1, 230),
(0, 'rain', 1, 231),
(10, 'rain', 1, 232),
(-1, 'rain', 1, 233),
(9, 'rain', 4, 234),
(0, 'rain', 4, 235),
(8, 'rain', 4, 236),
(1, 'rain', 4, 237),
(7, 'rain', 4, 238),
(2, 'rain', 4, 239),
(6, 'rain', 4, 240),
(3, 'rain', 4, 241),
(5, 'rain', 4, 242),
(4, 'rain', 4, 243),
(4, 'rain', 4, 244),
(0, 'rain', 4, 245),
(9, 'rain', 4, 246),
(1, 'rain', 4, 247),
(8, 'rain', 4, 248),
(2, 'rain', 4, 249),
(7, 'rain', 4, 250),
(3, 'rain', 4, 251),
(6, 'rain', 4, 252),
(4, 'rain', 4, 253),
(5, 'rain', 4, 254),
(5, 'rain', 4, 255),
(4, 'rain', 4, 256),
(6, 'rain', 4, 257),
(3, 'rain', 4, 258),
(7, 'rain', 4, 259),
(2, 'rain', 4, 260),
(8, 'rain', 4, 261),
(1, 'rain', 4, 262),
(9, 'rain', 4, 263),
(0, 'rain', 4, 264),
(10, 'rain', 4, 265),
(-1, 'rain', 4, 266),
(9, 'rain', 5, 267),
(0, 'rain', 5, 268),
(8, 'rain', 5, 269),
(1, 'rain', 5, 270),
(7, 'rain', 5, 271),
(2, 'rain', 5, 272),
(6, 'rain', 5, 273),
(3, 'rain', 5, 274),
(5, 'rain', 5, 275),
(4, 'rain', 5, 276),
(4, 'rain', 5, 277),
(0, 'rain', 5, 278),
(9, 'rain', 5, 279),
(1, 'rain', 5, 280),
(8, 'rain', 5, 281),
(2, 'rain', 5, 282),
(7, 'rain', 5, 283),
(3, 'rain', 5, 284),
(6, 'rain', 5, 285),
(4, 'rain', 5, 286),
(5, 'rain', 5, 287),
(5, 'rain', 5, 288),
(4, 'rain', 5, 289),
(6, 'rain', 5, 290),
(3, 'rain', 5, 291),
(7, 'rain', 5, 292),
(2, 'rain', 5, 293),
(8, 'rain', 5, 294),
(1, 'rain', 5, 295),
(9, 'rain', 5, 296),
(0, 'rain', 5, 297),
(10, 'rain', 5, 298),
(-1, 'rain', 5, 299),
(9, 'snow', 1, 300),
(0, 'snow', 1, 301),
(8, 'snow', 1, 302),
(1, 'snow', 1, 303),
(7, 'snow', 1, 304),
(2, 'snow', 1, 305),
(6, 'snow', 1, 306),
(3, 'snow', 1, 307),
(5, 'snow', 1, 308),
(4, 'snow', 1, 309),
(9, 'snow', 3, 310),
(0, 'snow', 3, 311),
(8, 'snow', 3, 312),
(1, 'snow', 3, 313),
(7, 'snow', 3, 314),
(2, 'snow', 3, 315),
(6, 'snow', 3, 316),
(3, 'snow', 3, 317),
(5, 'snow', 3, 318),
(4, 'snow', 3, 319),
(4, 'snow', 3, 320),
(0, 'snow', 3, 321),
(9, 'snow', 3, 322),
(1, 'snow', 3, 323),
(8, 'snow', 3, 324),
(2, 'snow', 3, 325),
(7, 'snow', 3, 326),
(3, 'snow', 3, 327),
(6, 'snow', 3, 328),
(4, 'snow', 3, 329),
(5, 'snow', 3, 330),
(5, 'snow', 3, 331),
(4, 'snow', 3, 332),
(6, 'snow', 3, 333),
(3, 'snow', 3, 334),
(7, 'snow', 3, 335),
(2, 'snow', 3, 336),
(8, 'snow', 3, 337),
(1, 'snow', 3, 338),
(9, 'snow', 3, 339),
(0, 'snow', 3, 340),
(10, 'snow', 3, 341),
(-1, 'snow', 3, 342),
(0, 'snow', 3, 343),
(9, 'snow', 3, 344),
(1, 'snow', 3, 345),
(8, 'snow', 3, 346),
(2, 'snow', 3, 347),
(7, 'snow', 3, 348),
(3, 'snow', 3, 349),
(6, 'snow', 3, 350),
(4, 'snow', 3, 351),
(5, 'snow', 3, 352),
(5, 'snow', 3, 353),
(4, 'snow', 3, 354),
(6, 'snow', 3, 355),
(3, 'snow', 3, 356),
(7, 'snow', 3, 357),
(2, 'snow', 3, 358),
(8, 'snow', 3, 359),
(1, 'snow', 3, 360),
(9, 'snow', 3, 361),
(0, 'snow', 3, 362),
(10, 'snow', 3, 363),
(-1, 'snow', 3, 364),
(0, 'snow', 3, 365),
(9, 'snow', 3, 366),
(1, 'snow', 3, 367),
(8, 'snow', 3, 368),
(2, 'snow', 3, 369),
(7, 'snow', 3, 370),
(3, 'snow', 3, 371),
(6, 'snow', 3, 372),
(4, 'snow', 3, 373),
(5, 'snow', 3, 374),
(5, 'snow', 3, 375),
(4, 'snow', 3, 376),
(6, 'snow', 3, 377),
(3, 'snow', 3, 378),
(7, 'snow', 3, 379),
(2, 'snow', 3, 380),
(8, 'snow', 3, 381),
(1, 'snow', 3, 382),
(9, 'snow', 3, 383),
(0, 'snow', 3, 384),
(10, 'snow', 3, 385),
(-1, 'snow', 3, 386),
(0, 'snow', 3, 387),
(9, 'snow', 3, 388),
(1, 'snow', 3, 389),
(9, 'snow', 5, 390),
(0, 'snow', 5, 391),
(8, 'snow', 5, 392),
(1, 'snow', 5, 393),
(7, 'snow', 5, 394),
(2, 'snow', 5, 395),
(6, 'snow', 5, 396),
(3, 'snow', 5, 397),
(5, 'snow', 5, 398),
(4, 'snow', 5, 399),
(19, 'clear_sky', 1, 400),
(10, 'clear_sky', 1, 401),
(18, 'clear_sky', 1, 402),
(11, 'clear_sky', 1, 403),
(17, 'clear_sky', 1, 404),
(12, 'clear_sky', 1, 405),
(16, 'clear_sky', 1, 406),
(13, 'clear_sky', 1, 407),
(15, 'clear_sky', 1, 408),
(14, 'clear_sky', 1, 409),
(14, 'clear_sky', 1, 410),
(10, 'clear_sky', 1, 411),
(19, 'clear_sky', 1, 412),
(11, 'clear_sky', 1, 413),
(18, 'clear_sky', 1, 414),
(12, 'clear_sky', 1, 415),
(17, 'clear_sky', 1, 416),
(13, 'clear_sky', 1, 417),
(16, 'clear_sky', 1, 418),
(14, 'clear_sky', 1, 419),
(15, 'clear_sky', 1, 420),
(15, 'clear_sky', 1, 421),
(14, 'clear_sky', 1, 422),
(16, 'clear_sky', 1, 423),
(13, 'clear_sky', 1, 424),
(17, 'clear_sky', 1, 425),
(12, 'clear_sky', 1, 426),
(18, 'clear_sky', 1, 427),
(11, 'clear_sky', 1, 428),
(19, 'clear_sky', 1, 429),
(19, 'clear_sky', 2, 430),
(10, 'clear_sky', 2, 431),
(18, 'clear_sky', 2, 432),
(11, 'clear_sky', 2, 433),
(17, 'clear_sky', 2, 434),
(12, 'clear_sky', 2, 435),
(16, 'clear_sky', 2, 436),
(13, 'clear_sky', 2, 437),
(15, 'clear_sky', 2, 438),
(14, 'clear_sky', 2, 439),
(14, 'clear_sky', 2, 440),
(10, 'clear_sky', 2, 441),
(19, 'clear_sky', 2, 442),
(11, 'clear_sky', 2, 443),
(18, 'clear_sky', 2, 444),
(12, 'clear_sky', 2, 445),
(17, 'clear_sky', 2, 446),
(13, 'clear_sky', 2, 447),
(16, 'clear_sky', 2, 448),
(14, 'clear_sky', 2, 449),
(15, 'clear_sky', 2, 450),
(15, 'clear_sky', 2, 451),
(14, 'clear_sky', 2, 452),
(16, 'clear_sky', 2, 453),
(13, 'clear_sky', 2, 454),
(17, 'clear_sky', 2, 455),
(12, 'clear_sky', 2, 456),
(18, 'clear_sky', 2, 457),
(11, 'clear_sky', 2, 458),
(19, 'clear_sky', 2, 459),
(19, 'clear_sky', 5, 460),
(10, 'clear_sky', 5, 461),
(18, 'clear_sky', 5, 462),
(11, 'clear_sky', 5, 463),
(17, 'clear_sky', 5, 464),
(12, 'clear_sky', 5, 465),
(16, 'clear_sky', 5, 466),
(13, 'clear_sky', 5, 467),
(15, 'clear_sky', 5, 468),
(14, 'clear_sky', 5, 469),
(14, 'clear_sky', 5, 470),
(10, 'clear_sky', 5, 471),
(19, 'clear_sky', 5, 472),
(11, 'clear_sky', 5, 473),
(18, 'clear_sky', 5, 474),
(12, 'clear_sky', 5, 475),
(17, 'clear_sky', 5, 476),
(13, 'clear_sky', 5, 477),
(16, 'clear_sky', 5, 478),
(14, 'clear_sky', 5, 479),
(15, 'clear_sky', 5, 480),
(15, 'clear_sky', 5, 481),
(14, 'clear_sky', 5, 482),
(16, 'clear_sky', 5, 483),
(13, 'clear_sky', 5, 484),
(17, 'clear_sky', 5, 485),
(12, 'clear_sky', 5, 486),
(18, 'clear_sky', 5, 487),
(11, 'clear_sky', 5, 488),
(19, 'clear_sky', 5, 489),
(10, 'clear_sky', 5, 490),
(20, 'clear_sky', 5, 491),
(9, 'clear_sky', 5, 492),
(10, 'clear_sky', 5, 493),
(19, 'clear_sky', 5, 494),
(11, 'clear_sky', 5, 495),
(18, 'clear_sky', 5, 496),
(12, 'clear_sky', 5, 497),
(17, 'clear_sky', 5, 498),
(13, 'clear_sky', 5, 499),
(19, 'scattered_clouds', 1, 500),
(10, 'scattered_clouds', 1, 501),
(18, 'scattered_clouds', 1, 502),
(11, 'scattered_clouds', 1, 503),
(17, 'scattered_clouds', 1, 504),
(12, 'scattered_clouds', 1, 505),
(16, 'scattered_clouds', 1, 506),
(13, 'scattered_clouds', 1, 507),
(15, 'scattered_clouds', 1, 508),
(14, 'scattered_clouds', 1, 509),
(14, 'scattered_clouds', 1, 510),
(10, 'scattered_clouds', 1, 511),
(19, 'scattered_clouds', 1, 512),
(11, 'scattered_clouds', 1, 513),
(18, 'scattered_clouds', 1, 514),
(12, 'scattered_clouds', 1, 515),
(17, 'scattered_clouds', 1, 516),
(13, 'scattered_clouds', 1, 517),
(16, 'scattered_clouds', 1, 518),
(14, 'scattered_clouds', 1, 519),
(15, 'scattered_clouds', 1, 520),
(15, 'scattered_clouds', 1, 521),
(14, 'scattered_clouds', 1, 522),
(16, 'scattered_clouds', 1, 523),
(13, 'scattered_clouds', 1, 524),
(17, 'scattered_clouds', 1, 525),
(12, 'scattered_clouds', 1, 526),
(18, 'scattered_clouds', 1, 527),
(11, 'scattered_clouds', 1, 528),
(19, 'scattered_clouds', 1, 529),
(10, 'scattered_clouds', 1, 530),
(20, 'scattered_clouds', 1, 531),
(9, 'scattered_clouds', 1, 532),
(10, 'scattered_clouds', 1, 533),
(19, 'scattered_clouds', 1, 534),
(11, 'scattered_clouds', 1, 535),
(18, 'scattered_clouds', 1, 536),
(12, 'scattered_clouds', 1, 537),
(17, 'scattered_clouds', 1, 538),
(13, 'scattered_clouds', 1, 539),
(16, 'scattered_clouds', 1, 540),
(14, 'scattered_clouds', 1, 541),
(15, 'scattered_clouds', 1, 542),
(15, 'scattered_clouds', 1, 543),
(14, 'scattered_clouds', 1, 544),
(16, 'scattered_clouds', 1, 545),
(13, 'scattered_clouds', 1, 546),
(17, 'scattered_clouds', 1, 547),
(12, 'scattered_clouds', 1, 548),
(18, 'scattered_clouds', 1, 549),
(19, 'scattered_clouds', 4, 550),
(10, 'scattered_clouds', 4, 551),
(18, 'scattered_clouds', 4, 552),
(11, 'scattered_clouds', 4, 553),
(17, 'scattered_clouds', 4, 554),
(12, 'scattered_clouds', 4, 555),
(16, 'scattered_clouds', 4, 556),
(13, 'scattered_clouds', 4, 557),
(15, 'scattered_clouds', 4, 558),
(14, 'scattered_clouds', 4, 559),
(14, 'scattered_clouds', 4, 560),
(10, 'scattered_clouds', 4, 561),
(19, 'scattered_clouds', 4, 562),
(11, 'scattered_clouds', 4, 563),
(18, 'scattered_clouds', 4, 564),
(12, 'scattered_clouds', 4, 565),
(17, 'scattered_clouds', 4, 566),
(13, 'scattered_clouds', 4, 567),
(16, 'scattered_clouds', 4, 568),
(14, 'scattered_clouds', 4, 569),
(19, 'scattered_clouds', 5, 570),
(10, 'scattered_clouds', 5, 571),
(18, 'scattered_clouds', 5, 572),
(11, 'scattered_clouds', 5, 573),
(17, 'scattered_clouds', 5, 574),
(12, 'scattered_clouds', 5, 575),
(16, 'scattered_clouds', 5, 576),
(13, 'scattered_clouds', 5, 577),
(15, 'scattered_clouds', 5, 578),
(14, 'scattered_clouds', 5, 579),
(14, 'scattered_clouds', 5, 580),
(10, 'scattered_clouds', 5, 581),
(19, 'scattered_clouds', 5, 582),
(11, 'scattered_clouds', 5, 583),
(18, 'scattered_clouds', 5, 584),
(12, 'scattered_clouds', 5, 585),
(17, 'scattered_clouds', 5, 586),
(13, 'scattered_clouds', 5, 587),
(16, 'scattered_clouds', 5, 588),
(14, 'scattered_clouds', 5, 589),
(15, 'scattered_clouds', 5, 590),
(15, 'scattered_clouds', 5, 591),
(14, 'scattered_clouds', 5, 592),
(16, 'scattered_clouds', 5, 593),
(13, 'scattered_clouds', 5, 594),
(17, 'scattered_clouds', 5, 595),
(12, 'scattered_clouds', 5, 596),
(18, 'scattered_clouds', 5, 597),
(11, 'scattered_clouds', 5, 598),
(19, 'scattered_clouds', 5, 599),
(19, 'rain', 1, 600),
(10, 'rain', 1, 601),
(18, 'rain', 1, 602),
(11, 'rain', 1, 603),
(17, 'rain', 1, 604),
(12, 'rain', 1, 605),
(16, 'rain', 1, 606),
(13, 'rain', 1, 607),
(15, 'rain', 1, 608),
(14, 'rain', 1, 609),
(14, 'rain', 1, 610),
(10, 'rain', 1, 611),
(19, 'rain', 1, 612),
(11, 'rain', 1, 613),
(18, 'rain', 1, 614),
(12, 'rain', 1, 615),
(17, 'rain', 1, 616),
(13, 'rain', 1, 617),
(16, 'rain', 1, 618),
(14, 'rain', 1, 619),
(15, 'rain', 1, 620),
(15, 'rain', 1, 621),
(14, 'rain', 1, 622),
(16, 'rain', 1, 623),
(13, 'rain', 1, 624),
(17, 'rain', 1, 625),
(12, 'rain', 1, 626),
(18, 'rain', 1, 627),
(11, 'rain', 1, 628),
(19, 'rain', 1, 629),
(10, 'rain', 1, 630),
(20, 'rain', 1, 631),
(9, 'rain', 1, 632),
(10, 'rain', 1, 633),
(19, 'rain', 1, 634),
(11, 'rain', 1, 635),
(18, 'rain', 1, 636),
(12, 'rain', 1, 637),
(17, 'rain', 1, 638),
(13, 'rain', 1, 639),
(16, 'rain', 1, 640),
(14, 'rain', 1, 641),
(15, 'rain', 1, 642),
(15, 'rain', 1, 643),
(14, 'rain', 1, 644),
(16, 'rain', 1, 645),
(13, 'rain', 1, 646),
(17, 'rain', 1, 647),
(12, 'rain', 1, 648),
(18, 'rain', 1, 649),
(11, 'rain', 1, 650),
(19, 'rain', 1, 651),
(10, 'rain', 1, 652),
(20, 'rain', 1, 653),
(9, 'rain', 1, 654),
(10, 'rain', 1, 655),
(19, 'rain', 1, 656),
(11, 'rain', 1, 657),
(18, 'rain', 1, 658),
(12, 'rain', 1, 659),
(19, 'rain', 4, 660),
(10, 'rain', 4, 661),
(18, 'rain', 4, 662),
(11, 'rain', 4, 663),
(17, 'rain', 4, 664),
(12, 'rain', 4, 665),
(16, 'rain', 4, 666),
(13, 'rain', 4, 667),
(15, 'rain', 4, 668),
(14, 'rain', 4, 669),
(14, 'rain', 4, 670),
(10, 'rain', 4, 671),
(19, 'rain', 4, 672),
(11, 'rain', 4, 673),
(18, 'rain', 4, 674),
(12, 'rain', 4, 675),
(17, 'rain', 4, 676),
(13, 'rain', 4, 677),
(16, 'rain', 4, 678),
(14, 'rain', 4, 679),
(19, 'rain', 5, 680),
(10, 'rain', 5, 681),
(18, 'rain', 5, 682),
(11, 'rain', 5, 683),
(17, 'rain', 5, 684),
(12, 'rain', 5, 685),
(16, 'rain', 5, 686),
(13, 'rain', 5, 687),
(15, 'rain', 5, 688),
(14, 'rain', 5, 689),
(14, 'rain', 5, 690),
(10, 'rain', 5, 691),
(19, 'rain', 5, 692),
(11, 'rain', 5, 693),
(18, 'rain', 5, 694),
(12, 'rain', 5, 695),
(17, 'rain', 5, 696),
(13, 'rain', 5, 697),
(16, 'rain', 5, 698),
(14, 'rain', 5, 699),
(29, 'clear_sky', 1, 700),
(20, 'clear_sky', 1, 701),
(28, 'clear_sky', 1, 702),
(21, 'clear_sky', 1, 703),
(27, 'clear_sky', 1, 704),
(29, 'clear_sky', 2, 705),
(20, 'clear_sky', 2, 706),
(28, 'clear_sky', 2, 707),
(21, 'clear_sky', 2, 708),
(27, 'clear_sky', 2, 709),
(22, 'clear_sky', 2, 710),
(26, 'clear_sky', 2, 711),
(23, 'clear_sky', 2, 712),
(25, 'clear_sky', 2, 713),
(24, 'clear_sky', 2, 714),
(24, 'clear_sky', 2, 715),
(20, 'clear_sky', 2, 716),
(29, 'clear_sky', 2, 717),
(21, 'clear_sky', 2, 718),
(28, 'clear_sky', 2, 719),
(22, 'clear_sky', 2, 720),
(27, 'clear_sky', 2, 721),
(23, 'clear_sky', 2, 722),
(26, 'clear_sky', 2, 723),
(24, 'clear_sky', 2, 724),
(25, 'clear_sky', 2, 725),
(25, 'clear_sky', 2, 726),
(24, 'clear_sky', 2, 727),
(26, 'clear_sky', 2, 728),
(23, 'clear_sky', 2, 729),
(27, 'clear_sky', 2, 730),
(22, 'clear_sky', 2, 731),
(28, 'clear_sky', 2, 732),
(21, 'clear_sky', 2, 733),
(29, 'clear_sky', 2, 734),
(20, 'clear_sky', 2, 735),
(30, 'clear_sky', 2, 736),
(19, 'clear_sky', 2, 737),
(20, 'clear_sky', 2, 738),
(29, 'clear_sky', 2, 739),
(21, 'clear_sky', 2, 740),
(28, 'clear_sky', 2, 741),
(22, 'clear_sky', 2, 742),
(27, 'clear_sky', 2, 743),
(23, 'clear_sky', 2, 744),
(26, 'clear_sky', 2, 745),
(24, 'clear_sky', 2, 746),
(25, 'clear_sky', 2, 747),
(25, 'clear_sky', 2, 748),
(24, 'clear_sky', 2, 749),
(26, 'clear_sky', 2, 750),
(23, 'clear_sky', 2, 751),
(27, 'clear_sky', 2, 752),
(22, 'clear_sky', 2, 753),
(28, 'clear_sky', 2, 754),
(21, 'clear_sky', 2, 755),
(29, 'clear_sky', 2, 756),
(20, 'clear_sky', 2, 757),
(30, 'clear_sky', 2, 758),
(19, 'clear_sky', 2, 759),
(20, 'clear_sky', 2, 760),
(29, 'clear_sky', 2, 761),
(21, 'clear_sky', 2, 762),
(28, 'clear_sky', 2, 763),
(22, 'clear_sky', 2, 764),
(29, 'clear_sky', 5, 765),
(20, 'clear_sky', 5, 766),
(28, 'clear_sky', 5, 767),
(21, 'clear_sky', 5, 768),
(27, 'clear_sky', 5, 769),
(22, 'clear_sky', 5, 770),
(26, 'clear_sky', 5, 771),
(23, 'clear_sky', 5, 772),
(25, 'clear_sky', 5, 773),
(24, 'clear_sky', 5, 774),
(24, 'clear_sky', 5, 775),
(20, 'clear_sky', 5, 776),
(29, 'clear_sky', 5, 777),
(21, 'clear_sky', 5, 778),
(28, 'clear_sky', 5, 779),
(22, 'clear_sky', 5, 780),
(27, 'clear_sky', 5, 781),
(23, 'clear_sky', 5, 782),
(26, 'clear_sky', 5, 783),
(24, 'clear_sky', 5, 784),
(25, 'clear_sky', 5, 785),
(25, 'clear_sky', 5, 786),
(24, 'clear_sky', 5, 787),
(26, 'clear_sky', 5, 788),
(23, 'clear_sky', 5, 789),
(27, 'clear_sky', 5, 790),
(22, 'clear_sky', 5, 791),
(28, 'clear_sky', 5, 792),
(21, 'clear_sky', 5, 793),
(29, 'clear_sky', 5, 794),
(20, 'clear_sky', 5, 795),
(30, 'clear_sky', 5, 796),
(19, 'clear_sky', 5, 797),
(20, 'clear_sky', 5, 798),
(29, 'clear_sky', 5, 799),
(29, 'scattered_clouds', 1, 800),
(20, 'scattered_clouds', 1, 801),
(28, 'scattered_clouds', 1, 802),
(21, 'scattered_clouds', 1, 803),
(27, 'scattered_clouds', 1, 804),
(29, 'scattered_clouds', 2, 805),
(20, 'scattered_clouds', 2, 806),
(28, 'scattered_clouds', 2, 807),
(21, 'scattered_clouds', 2, 808),
(27, 'scattered_clouds', 2, 809),
(22, 'scattered_clouds', 2, 810),
(26, 'scattered_clouds', 2, 811),
(23, 'scattered_clouds', 2, 812),
(25, 'scattered_clouds', 2, 813),
(24, 'scattered_clouds', 2, 814),
(24, 'scattered_clouds', 2, 815),
(20, 'scattered_clouds', 2, 816),
(29, 'scattered_clouds', 2, 817),
(21, 'scattered_clouds', 2, 818),
(28, 'scattered_clouds', 2, 819),
(22, 'scattered_clouds', 2, 820),
(27, 'scattered_clouds', 2, 821),
(23, 'scattered_clouds', 2, 822),
(26, 'scattered_clouds', 2, 823),
(24, 'scattered_clouds', 2, 824),
(25, 'scattered_clouds', 2, 825),
(25, 'scattered_clouds', 2, 826),
(24, 'scattered_clouds', 2, 827),
(26, 'scattered_clouds', 2, 828),
(23, 'scattered_clouds', 2, 829),
(27, 'scattered_clouds', 2, 830),
(22, 'scattered_clouds', 2, 831),
(28, 'scattered_clouds', 2, 832),
(21, 'scattered_clouds', 2, 833),
(29, 'scattered_clouds', 2, 834),
(20, 'scattered_clouds', 2, 835),
(30, 'scattered_clouds', 2, 836),
(19, 'scattered_clouds', 2, 837),
(20, 'scattered_clouds', 2, 838),
(29, 'scattered_clouds', 2, 839),
(21, 'scattered_clouds', 2, 840),
(28, 'scattered_clouds', 2, 841),
(22, 'scattered_clouds', 2, 842),
(27, 'scattered_clouds', 2, 843),
(23, 'scattered_clouds', 2, 844),
(26, 'scattered_clouds', 2, 845),
(24, 'scattered_clouds', 2, 846),
(25, 'scattered_clouds', 2, 847),
(25, 'scattered_clouds', 2, 848),
(24, 'scattered_clouds', 2, 849),
(26, 'scattered_clouds', 2, 850),
(23, 'scattered_clouds', 2, 851),
(27, 'scattered_clouds', 2, 852),
(22, 'scattered_clouds', 2, 853),
(28, 'scattered_clouds', 2, 854),
(29, 'scattered_clouds', 5, 855),
(20, 'scattered_clouds', 5, 856),
(28, 'scattered_clouds', 5, 857),
(21, 'scattered_clouds', 5, 858),
(27, 'scattered_clouds', 5, 859),
(22, 'scattered_clouds', 5, 860),
(26, 'scattered_clouds', 5, 861),
(23, 'scattered_clouds', 5, 862),
(25, 'scattered_clouds', 5, 863),
(24, 'scattered_clouds', 5, 864),
(24, 'scattered_clouds', 5, 865),
(20, 'scattered_clouds', 5, 866),
(29, 'scattered_clouds', 5, 867),
(21, 'scattered_clouds', 5, 868),
(28, 'scattered_clouds', 5, 869),
(22, 'scattered_clouds', 5, 870),
(27, 'scattered_clouds', 5, 871),
(23, 'scattered_clouds', 5, 872),
(26, 'scattered_clouds', 5, 873),
(24, 'scattered_clouds', 5, 874),
(25, 'scattered_clouds', 5, 875),
(25, 'scattered_clouds', 5, 876),
(24, 'scattered_clouds', 5, 877),
(26, 'scattered_clouds', 5, 878),
(23, 'scattered_clouds', 5, 879),
(27, 'scattered_clouds', 5, 880),
(22, 'scattered_clouds', 5, 881),
(28, 'scattered_clouds', 5, 882),
(21, 'scattered_clouds', 5, 883),
(29, 'scattered_clouds', 5, 884),
(20, 'scattered_clouds', 5, 885),
(30, 'scattered_clouds', 5, 886),
(19, 'scattered_clouds', 5, 887),
(20, 'scattered_clouds', 5, 888),
(29, 'scattered_clouds', 5, 889),
(21, 'scattered_clouds', 5, 890),
(28, 'scattered_clouds', 5, 891),
(22, 'scattered_clouds', 5, 892),
(27, 'scattered_clouds', 5, 893),
(23, 'scattered_clouds', 5, 894),
(26, 'scattered_clouds', 5, 895),
(24, 'scattered_clouds', 5, 896),
(25, 'scattered_clouds', 5, 897),
(25, 'scattered_clouds', 5, 898),
(24, 'scattered_clouds', 5, 899),
(29, 'rain', 1, 900),
(20, 'rain', 1, 901),
(28, 'rain', 1, 902),
(21, 'rain', 1, 903),
(27, 'rain', 1, 904),
(22, 'rain', 1, 905),
(26, 'rain', 1, 906),
(23, 'rain', 1, 907),
(25, 'rain', 1, 908),
(24, 'rain', 1, 909),
(24, 'rain', 1, 910),
(20, 'rain', 1, 911),
(29, 'rain', 1, 912),
(21, 'rain', 1, 913),
(28, 'rain', 1, 914),
(22, 'rain', 1, 915),
(27, 'rain', 1, 916),
(23, 'rain', 1, 917),
(26, 'rain', 1, 918),
(24, 'rain', 1, 919),
(25, 'rain', 1, 920),
(25, 'rain', 1, 921),
(24, 'rain', 1, 922),
(26, 'rain', 1, 923),
(23, 'rain', 1, 924),
(27, 'rain', 1, 925),
(22, 'rain', 1, 926),
(28, 'rain', 1, 927),
(21, 'rain', 1, 928),
(29, 'rain', 1, 929),
(20, 'rain', 1, 930),
(30, 'rain', 1, 931),
(19, 'rain', 1, 932),
(20, 'rain', 1, 933),
(29, 'rain', 1, 934),
(21, 'rain', 1, 935),
(28, 'rain', 1, 936),
(22, 'rain', 1, 937),
(27, 'rain', 1, 938),
(23, 'rain', 1, 939),
(26, 'rain', 1, 940),
(24, 'rain', 1, 941),
(25, 'rain', 1, 942),
(25, 'rain', 1, 943),
(24, 'rain', 1, 944),
(29, 'rain', 4, 945),
(20, 'rain', 4, 946),
(28, 'rain', 4, 947),
(21, 'rain', 4, 948),
(27, 'rain', 4, 949),
(29, 'rain', 5, 950),
(20, 'rain', 5, 951),
(28, 'rain', 5, 952),
(21, 'rain', 5, 953),
(27, 'rain', 5, 954),
(22, 'rain', 5, 955),
(26, 'rain', 5, 956),
(23, 'rain', 5, 957),
(25, 'rain', 5, 958),
(24, 'rain', 5, 959),
(24, 'rain', 5, 960),
(20, 'rain', 5, 961),
(29, 'rain', 5, 962),
(21, 'rain', 5, 963),
(28, 'rain', 5, 964),
(22, 'rain', 5, 965),
(27, 'rain', 5, 966),
(23, 'rain', 5, 967),
(26, 'rain', 5, 968),
(24, 'rain', 5, 969),
(25, 'rain', 5, 970),
(25, 'rain', 5, 971),
(24, 'rain', 5, 972),
(26, 'rain', 5, 973),
(23, 'rain', 5, 974),
(27, 'rain', 5, 975),
(22, 'rain', 5, 976),
(28, 'rain', 5, 977),
(21, 'rain', 5, 978),
(29, 'rain', 5, 979),
(20, 'rain', 5, 980),
(30, 'rain', 5, 981),
(19, 'rain', 5, 982),
(20, 'rain', 5, 983),
(29, 'rain', 5, 984),
(21, 'rain', 5, 985),
(28, 'rain', 5, 986),
(22, 'rain', 5, 987),
(27, 'rain', 5, 988),
(23, 'rain', 5, 989),
(26, 'rain', 5, 990),
(24, 'rain', 5, 991),
(25, 'rain', 5, 992),
(25, 'rain', 5, 993),
(24, 'rain', 5, 994),
(26, 'rain', 5, 995),
(23, 'rain', 5, 996),
(27, 'rain', 5, 997),
(22, 'rain', 5, 998),
(28, 'rain', 5, 999),
(39, 'clear_sky', 1, 1000),
(30, 'clear_sky', 1, 1001),
(38, 'clear_sky', 1, 1002),
(31, 'clear_sky', 1, 1003),
(37, 'clear_sky', 1, 1004),
(32, 'clear_sky', 1, 1005),
(36, 'clear_sky', 1, 1006),
(33, 'clear_sky', 1, 1007),
(35, 'clear_sky', 1, 1008),
(34, 'clear_sky', 1, 1009),
(34, 'clear_sky', 1, 1010),
(30, 'clear_sky', 1, 1011),
(39, 'clear_sky', 1, 1012),
(31, 'clear_sky', 1, 1013),
(38, 'clear_sky', 1, 1014),
(32, 'clear_sky', 1, 1015),
(37, 'clear_sky', 1, 1016),
(33, 'clear_sky', 1, 1017),
(36, 'clear_sky', 1, 1018),
(34, 'clear_sky', 1, 1019),
(35, 'clear_sky', 1, 1020),
(35, 'clear_sky', 1, 1021),
(34, 'clear_sky', 1, 1022),
(36, 'clear_sky', 1, 1023),
(33, 'clear_sky', 1, 1024),
(37, 'clear_sky', 1, 1025),
(32, 'clear_sky', 1, 1026),
(38, 'clear_sky', 1, 1027),
(31, 'clear_sky', 1, 1028),
(39, 'clear_sky', 1, 1029),
(30, 'clear_sky', 1, 1030),
(40, 'clear_sky', 1, 1031),
(29, 'clear_sky', 1, 1032),
(30, 'clear_sky', 1, 1033),
(39, 'clear_sky', 1, 1034),
(31, 'clear_sky', 1, 1035),
(38, 'clear_sky', 1, 1036),
(32, 'clear_sky', 1, 1037),
(37, 'clear_sky', 1, 1038),
(33, 'clear_sky', 1, 1039),
(36, 'clear_sky', 1, 1040),
(34, 'clear_sky', 1, 1041),
(35, 'clear_sky', 1, 1042),
(35, 'clear_sky', 1, 1043),
(34, 'clear_sky', 1, 1044),
(36, 'clear_sky', 1, 1045),
(33, 'clear_sky', 1, 1046),
(37, 'clear_sky', 1, 1047),
(32, 'clear_sky', 1, 1048),
(38, 'clear_sky', 1, 1049),
(39, 'clear_sky', 2, 1050),
(30, 'clear_sky', 2, 1051),
(38, 'clear_sky', 2, 1052),
(31, 'clear_sky', 2, 1053),
(37, 'clear_sky', 2, 1054),
(32, 'clear_sky', 2, 1055),
(36, 'clear_sky', 2, 1056),
(33, 'clear_sky', 2, 1057),
(35, 'clear_sky', 2, 1058),
(34, 'clear_sky', 2, 1059),
(39, 'clear_sky', 5, 1060),
(30, 'clear_sky', 5, 1061),
(38, 'clear_sky', 5, 1062),
(31, 'clear_sky', 5, 1063),
(37, 'clear_sky', 5, 1064),
(32, 'clear_sky', 5, 1065),
(36, 'clear_sky', 5, 1066),
(33, 'clear_sky', 5, 1067),
(35, 'clear_sky', 5, 1068),
(34, 'clear_sky', 5, 1069),
(34, 'clear_sky', 5, 1070),
(30, 'clear_sky', 5, 1071),
(39, 'clear_sky', 5, 1072),
(31, 'clear_sky', 5, 1073),
(38, 'clear_sky', 5, 1074),
(32, 'clear_sky', 5, 1075),
(37, 'clear_sky', 5, 1076),
(33, 'clear_sky', 5, 1077),
(36, 'clear_sky', 5, 1078),
(34, 'clear_sky', 5, 1079),
(35, 'clear_sky', 5, 1080),
(35, 'clear_sky', 5, 1081),
(34, 'clear_sky', 5, 1082),
(36, 'clear_sky', 5, 1083),
(33, 'clear_sky', 5, 1084),
(37, 'clear_sky', 5, 1085),
(32, 'clear_sky', 5, 1086),
(38, 'clear_sky', 5, 1087),
(31, 'clear_sky', 5, 1088),
(39, 'clear_sky', 5, 1089),
(30, 'clear_sky', 5, 1090),
(40, 'clear_sky', 5, 1091),
(29, 'clear_sky', 5, 1092),
(30, 'clear_sky', 5, 1093),
(39, 'clear_sky', 5, 1094),
(31, 'clear_sky', 5, 1095),
(38, 'clear_sky', 5, 1096),
(32, 'clear_sky', 5, 1097),
(37, 'clear_sky', 5, 1098),
(33, 'clear_sky', 5, 1099),
(39, 'scattered_clouds', 1, 1100),
(30, 'scattered_clouds', 1, 1101),
(38, 'scattered_clouds', 1, 1102),
(31, 'scattered_clouds', 1, 1103),
(37, 'scattered_clouds', 1, 1104),
(32, 'scattered_clouds', 1, 1105),
(36, 'scattered_clouds', 1, 1106),
(33, 'scattered_clouds', 1, 1107),
(35, 'scattered_clouds', 1, 1108),
(34, 'scattered_clouds', 1, 1109),
(34, 'scattered_clouds', 1, 1110),
(30, 'scattered_clouds', 1, 1111),
(39, 'scattered_clouds', 1, 1112),
(31, 'scattered_clouds', 1, 1113),
(38, 'scattered_clouds', 1, 1114),
(32, 'scattered_clouds', 1, 1115),
(37, 'scattered_clouds', 1, 1116),
(33, 'scattered_clouds', 1, 1117),
(36, 'scattered_clouds', 1, 1118),
(34, 'scattered_clouds', 1, 1119),
(35, 'scattered_clouds', 1, 1120),
(35, 'scattered_clouds', 1, 1121),
(34, 'scattered_clouds', 1, 1122),
(36, 'scattered_clouds', 1, 1123),
(33, 'scattered_clouds', 1, 1124),
(37, 'scattered_clouds', 1, 1125),
(32, 'scattered_clouds', 1, 1126),
(38, 'scattered_clouds', 1, 1127),
(31, 'scattered_clouds', 1, 1128),
(39, 'scattered_clouds', 1, 1129),
(30, 'scattered_clouds', 1, 1130),
(40, 'scattered_clouds', 1, 1131),
(29, 'scattered_clouds', 1, 1132),
(30, 'scattered_clouds', 1, 1133),
(39, 'scattered_clouds', 1, 1134),
(31, 'scattered_clouds', 1, 1135),
(38, 'scattered_clouds', 1, 1136),
(32, 'scattered_clouds', 1, 1137),
(37, 'scattered_clouds', 1, 1138),
(33, 'scattered_clouds', 1, 1139),
(39, 'scattered_clouds', 2, 1140),
(30, 'scattered_clouds', 2, 1141),
(38, 'scattered_clouds', 2, 1142),
(31, 'scattered_clouds', 2, 1143),
(37, 'scattered_clouds', 2, 1144),
(32, 'scattered_clouds', 2, 1145),
(36, 'scattered_clouds', 2, 1146),
(33, 'scattered_clouds', 2, 1147),
(35, 'scattered_clouds', 2, 1148),
(34, 'scattered_clouds', 2, 1149),
(34, 'scattered_clouds', 2, 1150),
(30, 'scattered_clouds', 2, 1151),
(39, 'scattered_clouds', 2, 1152),
(31, 'scattered_clouds', 2, 1153),
(38, 'scattered_clouds', 2, 1154),
(32, 'scattered_clouds', 2, 1155),
(37, 'scattered_clouds', 2, 1156),
(33, 'scattered_clouds', 2, 1157),
(36, 'scattered_clouds', 2, 1158),
(34, 'scattered_clouds', 2, 1159),
(35, 'scattered_clouds', 2, 1160),
(35, 'scattered_clouds', 2, 1161),
(34, 'scattered_clouds', 2, 1162),
(36, 'scattered_clouds', 2, 1163),
(33, 'scattered_clouds', 2, 1164),
(37, 'scattered_clouds', 2, 1165),
(32, 'scattered_clouds', 2, 1166),
(38, 'scattered_clouds', 2, 1167),
(31, 'scattered_clouds', 2, 1168),
(39, 'scattered_clouds', 2, 1169),
(39, 'scattered_clouds', 5, 1170),
(30, 'scattered_clouds', 5, 1171),
(38, 'scattered_clouds', 5, 1172),
(31, 'scattered_clouds', 5, 1173),
(37, 'scattered_clouds', 5, 1174),
(32, 'scattered_clouds', 5, 1175),
(36, 'scattered_clouds', 5, 1176),
(33, 'scattered_clouds', 5, 1177),
(35, 'scattered_clouds', 5, 1178),
(34, 'scattered_clouds', 5, 1179),
(34, 'scattered_clouds', 5, 1180),
(30, 'scattered_clouds', 5, 1181),
(39, 'scattered_clouds', 5, 1182),
(31, 'scattered_clouds', 5, 1183),
(38, 'scattered_clouds', 5, 1184),
(32, 'scattered_clouds', 5, 1185),
(37, 'scattered_clouds', 5, 1186),
(33, 'scattered_clouds', 5, 1187),
(36, 'scattered_clouds', 5, 1188),
(34, 'scattered_clouds', 5, 1189),
(35, 'scattered_clouds', 5, 1190),
(35, 'scattered_clouds', 5, 1191),
(34, 'scattered_clouds', 5, 1192),
(36, 'scattered_clouds', 5, 1193),
(33, 'scattered_clouds', 5, 1194),
(37, 'scattered_clouds', 5, 1195),
(32, 'scattered_clouds', 5, 1196),
(38, 'scattered_clouds', 5, 1197),
(31, 'scattered_clouds', 5, 1198),
(39, 'scattered_clouds', 5, 1199),
(39, 'rain', 1, 1200),
(30, 'rain', 1, 1201),
(38, 'rain', 1, 1202),
(31, 'rain', 1, 1203),
(37, 'rain', 1, 1204),
(32, 'rain', 1, 1205),
(36, 'rain', 1, 1206),
(33, 'rain', 1, 1207),
(35, 'rain', 1, 1208),
(34, 'rain', 1, 1209),
(34, 'rain', 1, 1210),
(30, 'rain', 1, 1211),
(39, 'rain', 1, 1212),
(31, 'rain', 1, 1213),
(38, 'rain', 1, 1214),
(32, 'rain', 1, 1215),
(37, 'rain', 1, 1216),
(33, 'rain', 1, 1217),
(36, 'rain', 1, 1218),
(34, 'rain', 1, 1219),
(35, 'rain', 1, 1220),
(35, 'rain', 1, 1221),
(34, 'rain', 1, 1222),
(36, 'rain', 1, 1223),
(33, 'rain', 1, 1224),
(37, 'rain', 1, 1225),
(32, 'rain', 1, 1226),
(38, 'rain', 1, 1227),
(31, 'rain', 1, 1228),
(39, 'rain', 1, 1229),
(30, 'rain', 1, 1230),
(40, 'rain', 1, 1231),
(29, 'rain', 1, 1232),
(30, 'rain', 1, 1233),
(39, 'rain', 1, 1234),
(31, 'rain', 1, 1235),
(38, 'rain', 1, 1236),
(32, 'rain', 1, 1237),
(37, 'rain', 1, 1238),
(33, 'rain', 1, 1239),
(36, 'rain', 1, 1240),
(34, 'rain', 1, 1241),
(35, 'rain', 1, 1242),
(35, 'rain', 1, 1243),
(34, 'rain', 1, 1244),
(36, 'rain', 1, 1245),
(33, 'rain', 1, 1246),
(37, 'rain', 1, 1247),
(32, 'rain', 1, 1248),
(38, 'rain', 1, 1249),
(31, 'rain', 1, 1250),
(39, 'rain', 1, 1251),
(30, 'rain', 1, 1252),
(40, 'rain', 1, 1253),
(29, 'rain', 1, 1254),
(30, 'rain', 1, 1255),
(39, 'rain', 1, 1256),
(31, 'rain', 1, 1257),
(38, 'rain', 1, 1258),
(32, 'rain', 1, 1259),
(37, 'rain', 1, 1260),
(33, 'rain', 1, 1261),
(36, 'rain', 1, 1262),
(34, 'rain', 1, 1263),
(35, 'rain', 1, 1264),
(35, 'rain', 1, 1265),
(34, 'rain', 1, 1266),
(36, 'rain', 1, 1267),
(33, 'rain', 1, 1268),
(37, 'rain', 1, 1269),
(39, 'rain', 4, 1270),
(30, 'rain', 4, 1271),
(38, 'rain', 4, 1272),
(31, 'rain', 4, 1273),
(37, 'rain', 4, 1274),
(39, 'rain', 5, 1275),
(30, 'rain', 5, 1276),
(38, 'rain', 5, 1277),
(31, 'rain', 5, 1278),
(37, 'rain', 5, 1279),
(32, 'rain', 5, 1280),
(36, 'rain', 5, 1281),
(33, 'rain', 5, 1282),
(35, 'rain', 5, 1283),
(34, 'rain', 5, 1284),
(34, 'rain', 5, 1285),
(30, 'rain', 5, 1286),
(39, 'rain', 5, 1287),
(31, 'rain', 5, 1288),
(38, 'rain', 5, 1289),
(32, 'rain', 5, 1290),
(37, 'rain', 5, 1291),
(33, 'rain', 5, 1292),
(36, 'rain', 5, 1293),
(34, 'rain', 5, 1294),
(35, 'rain', 5, 1295),
(35, 'rain', 5, 1296),
(34, 'rain', 5, 1297),
(36, 'rain', 5, 1298),
(33, 'rain', 5, 1299);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
